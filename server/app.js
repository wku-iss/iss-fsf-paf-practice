const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");
const fs = require("fs");
const multer = require("multer");
const uuid = require("uuid/v4");


const session = require("express-session");
const watch = require("connect-ensure-login");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

const authenticate = function (username, password, done) {
    // console.log(username + ":" + password);
    // const valid = false;
    Account.findOne({
        where: {
            email: username,
            password, password
        }
    }).then(valid => {
        if (valid) {
            return done(null, username);
        } else {
            return done(null, false);
        }
    });

}

passport.use(new LocalStrategy({
    usernameField: "email",
    passwordField: "password"
}, authenticate));

passport.serializeUser(function (username, done) {
    Account.findOne({ where: { email: username } }).then(found => {
        if (found) {
            done(null, found.id);
        }
    })
});

passport.deserializeUser(function (id, done) {
    // var userObject = {
    //     email: id
    // }
    // done(null, userObject);
    Account.findById(id).then(found => {
        if (found) {
            const user = {
                id: found.id
            }
            done(null, user);
        }
    })

});

// const mailgun = require("mailgun-js");
const api_key = 'key-82c503e4602cbe9de6a60b39e7390c6b';
const DOMAIN = 'mg.wkupteltd.com';
const mailgun = require('mailgun-js')({ apiKey: api_key, domain: DOMAIN });

// var data = {
//   from: 'Excited User <me@samples.mailgun.org>',
//   to: 'kucheech@gmail.com',
//   subject: 'Hello',
//   text: 'Testing some Mailgun awesomness!'
// };

// mailgun.messages().send(data, function (error, body) {
//   console.log(body);
// });


//start sequelize
const DB_NAME = "iss-fsf-paf";
const DB_USER = "root";
const DB_PASSWORD = "password";
const DB_HOST = "localhost"
const Sequelize = require("sequelize");
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD,
    {
        host: DB_HOST,
        dialect: 'mysql',
        logging: console.log,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: false
        }
    }
);
const Account = require("./models/account")(sequelize, Sequelize);
const Post = require("./models/post")(sequelize, Sequelize);
Post.belongsTo(Account, { foreignKey: 'postedby' });
// Post.hasOne(Account);
// Post.hasOne(Account, { foreignKey: 'postedby' });



const app = express();
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json({ limit: "50mb" }));


app.use(session({
    secret: "iss-fsf",
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));

app.use("/protected", watch.ensureLoggedIn("/status/401"));


const POSTS_IMAGES_FOLDER = path.join(__dirname, "/images/posts/");
app.use("/protected/posts/images/", express.static(POSTS_IMAGES_FOLDER));


const AVATARS_FOLDER = path.join(__dirname, "/images/avatars/");
// app.use("/protected/avatars", express.static(AVATARS_FOLDER));



app.get("/protected/avatars/:id", function (req, res) {
    const id = req.params.id;
    // console.log(id);
    const imgpath = path.resolve(AVATARS_FOLDER, (id + ".jpg"));
    if (fs.existsSync(imgpath)) {
        res.status(200).sendFile(imgpath);
    } else {
        res.status(200).sendFile(path.resolve(AVATARS_FOLDER, "default.jpg"));
    }
});




app.post("/login", passport.authenticate("local", {
    successRedirect: "/status/202",
    failureRedirect: "/status/403"
}));

app.get("/logout", function (req, res) {
    req.logout();
    req.session.destroy();
    res.status(200).end();
});

app.get("/status/:code", function (req, res) {
    const code = parseInt(req.params.code);
    res.sendStatus(code);
});

app.get("/protected/accounts", function (req, res) {
    Account.findAll().then(accounts => {
        res.status(200).json(accounts);
    })
});

app.get("/protected/posts", function (req, res) {
    console.log(req.user);
    Post.findAll({ order: [["createdAt", "DESC"]], include: [{ model: Account, attributes: ["name"] }] }).then(posts => {
        res.status(200).json(posts);
    })
});

app.post("/protected/posts", function (req, res) {
    // console.log(req.user);
    const id = req.user.id;
    // console.log(req.body.post);
    var post = req.body.post;
    post.postedby = id;
    Post.create(post).then(newpost => {
        if(newpost) {
            res.status(201).end();
        } else {
            res.status(500).end();
        }
    })
});


const storage = multer.diskStorage({
    destination: POSTS_IMAGES_FOLDER,
    filename: function (req, file, callback) {
        // console.log(req);
        // console.log(file);
        callback(null, Date.now() + "-" + file.originalname)
    }
});

const upload = multer({
    storage: storage
})

app.post("/protected/upload/", upload.single("img-file"), function (req, res) {
    // console.log(req);
    // console.log("upload");
    const id = req.user.id;
    const filename = uuid() + ".jpg";
    const imgpath = path.resolve(POSTS_IMAGES_FOLDER, filename);
    fs.rename(req.file.path, imgpath, function(err) {
        if (err) {
            console.log("Error: " + err);
        }

        res.status(202).send(filename);
    })
});




//add account
app.post("/resetpassword", function (req, res) {
    const email = req.body.email;

    Account.findOne({ where: { email: email } })
        .then(found => {
            if (found) {
                var data = {
                    from: "donotreply@wkupteltd.com",
                    to: email,
                    subject: "Reset password",
                    text: "Hi " + found.name + ",\n\n\nWe have received a request to reset your password.\n\n\nIf you did not make this request or believe this was received in error, please ignore this message."
                };

                mailgun.messages().send(data, function (error, body) {
                    if (error) {
                        console.log(error);
                        console.log(body);
                    } else {
                        console.log(body);
                        res.status(200).send("Reset password email has been sent to " + email);
                    }
                });
            } else {
                res.status(400).send("User does not exist in database");
            }
        }).catch(err => {
            handleError(err, res);
        });

});



app.get("/accounts", function (req, res) {
    Account.findAll().then(accounts => {
        res.status(200).json(accounts);
    })
});

app.get("/accounts/:id", function (req, res) {
    const id = req.params.id;
    Account.findById(id).then(account => {
        res.status(200).json(account);
    }).catch(err => {
        handleError(err, res);
    });
});

//add account
app.post("/accounts", function (req, res) {
    const account = req.body.account;
    console.log(account);
    Account.findOrCreate({ where: { email: account.email }, defaults: account })
        .spread((a, created) => {
            if (created) {
                var data = {
                    from: "donotreply@wkupteltd.com",
                    to: account.email,
                    subject: "Account created",
                    text: "Hi " + account.name + ",\n\n\nWe have received a request to create an account.\n\n\nIf you did not make this request or believe this was received in error, please ignore this message."
                };

                mailgun.messages().send(data, function (error, body) {
                    if (error) {
                        console.log(error);
                        console.log(body);
                        res.status(201).send("Account added to database");
                    } else {
                        console.log(body);
                        res.status(201).send("Account added to database and email sent to " + a.email);
                    }
                });
            } else {
                res.status(422).send("Email already exists in database");
            }
        }).catch(err => {
            handleError(err, res);
        });

});


//update account
app.put("/accounts/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    const account = req.body.account;

    Account.update(account, { where: { id: id } }).then(result => {
        res.status(200).send(result);
    }).catch(err => {
        console.log(err);
        res.status(500).send(err);
    });
});

//delete a particular account based on id
app.delete("/accounts/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    // console.log(id);
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    Account.destroy({ where: { id: id } }).then(result => {
        res.status(200).end();
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
});


var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'iss-fsf'
});


// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

const SELECT_ALL_USERS = "select * from users";
const SELECT_USER_BY_ID = "select * from users where regid = ? limit 1";
const UPDATE_USER_BY_ID = "update users set name = ?, email = ?, phone = ?, dob = ? where regid = ?";


const getAllUsers = mkQuery(SELECT_ALL_USERS, pool);
const getUserById = mkQuery(SELECT_USER_BY_ID, pool);
const updateUserById = mkQuery(UPDATE_USER_BY_ID, pool);

function formatDate(date1) {
    return date1.getFullYear() + '-' +
        (date1.getMonth() < 9 ? '0' : "") + (date1.getMonth() + 1) + '-' +
        (date1.getDate() < 10 ? '0' : "") + date1.getDate();
}

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});


app.post("/user/:id", function (req, res) {
    const id = req.params.id;
    const user = req.body.user;
    updateUserById(user.name, user.email, user.phone, formatDate(new Date(user.dob)), id)
        .then(function (result) {
            console.log(result);
            // res.status(404).type("text/plain").send("Customer not found");
            res.status(200).end();
        }).catch(function (err) {
            handleError(err, res);
        });
});



app.get("/users", function (req, res) {
    getAllUsers().then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("No users");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.get("/user/:id", function (req, res) {
    const id = req.params.id;
    getUserById(id).then(function (result) {
        if (result.length > 0) {
            var book = result[0];
            res.status(200).json(book);
        } else {
            res.status(404).send("User not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app