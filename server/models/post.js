module.exports = function (sequelize, Sequelize) {
    const Post = sequelize.define("posts",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            text: {
                type: Sequelize.STRING,
                allowNull: false
            },
            image: {
                type: Sequelize.STRING,
                allowNull: true
            },
            postedby: {
                type: Sequelize.INTEGER,
                allowNull: false
            }
        }, {
            tableName: "posts",
            timestamps: true   //add timestamps attributes updatedAt and createdAt            
        });

    return Post;
};