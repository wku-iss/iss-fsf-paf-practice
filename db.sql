-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: iss-fsf-paf
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'Alice','a@a.com','1111'),(2,'Bob','b@b.com','1234'),(3,'William','kucheech@gmail.com','1234'),(5,'William','wku@kaplan.com','1234');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `postedby` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_idx` (`postedby`),
  CONSTRAINT `fk_postedby` FOREIGN KEY (`postedby`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'testing','avengers.jpg',3,NULL,NULL),(2,'hihih',NULL,1,NULL,NULL),(3,'hahaha','minions.png',2,NULL,NULL),(4,'this is a test','',1,NULL,NULL),(5,'8787','',1,NULL,NULL),(6,'jskjdaskdjklas','',1,'2017-09-29 07:27:46','2017-09-29 07:27:46'),(7,'dfjsdkfjsd','',1,'2017-09-29 07:34:21','2017-09-29 07:34:21'),(8,'another test','',3,'2017-09-29 07:40:08','2017-09-29 07:40:08'),(9,'test','',3,'2017-09-29 07:46:24','2017-09-29 07:46:24'),(10,'hello','',1,'2017-09-29 08:03:22','2017-09-29 08:03:22'),(11,'jkjkj',NULL,1,'2017-09-29 08:35:20','2017-09-29 08:35:20'),(12,'ikolkk','',1,'2017-09-29 08:35:24','2017-09-29 08:35:24'),(13,'jkjjkj','',1,'2017-09-29 08:35:36','2017-09-29 08:35:36'),(14,'111111',NULL,1,'2017-09-29 08:38:00','2017-09-29 08:38:00'),(15,'ioioioi',NULL,1,'2017-09-29 08:44:26','2017-09-29 08:44:26'),(16,'testt',NULL,1,'2017-09-29 08:48:43','2017-09-29 08:48:43'),(17,'jkjkljkljkl','93f2a23d-1a83-4fc8-8444-de7127cd69a3.jpg',1,'2017-09-29 08:49:50','2017-09-29 08:49:50'),(18,'hahahahah','7292641e-7fb2-4249-8162-814ee2711941.jpg',1,'2017-09-29 08:50:26','2017-09-29 08:50:26');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-29 19:26:06
