(function () {
    "use strict";

    angular
        .module("MyApp")
        .controller("PostsCtrl", PostsCtrl);

    PostsCtrl.$inject = ["$scope", "MyAppService", "$state", "authService"];
    function PostsCtrl($scope, MyAppService, $state, authService) {
        var vm = this;
        vm.posts = [];
        vm.file = "";
        vm.newpost = {
            text: "",
            image: ""
        }
        vm.submittingPost = false;
        vm.submitPost = submitPost;
        // vm.createPost = createPost;
        vm.logout = logout;

        init();
        ///////////////////////

        $scope.$on("event:auth-loginRequired", function () {
            console.log("401");
            $state.go("login");
        });

        $scope.$on("event:auth-loginConfirmed", function () {
            console.log("202");
            $state.go("posts");
        });

        $scope.$on("event:auth-forbidden", function () {
            console.log("403");
        });

        // function submitPost() {

        //     if (vm.file != "") {
        //         MyAppService.uploadImage(vm.file).then(function (result) {
        //             // console.log(result);
        //             vm.newpost.image = result;
        //             vm.createPost();
        //         }).catch(function (err) {
        //             console.log(err)
        //         });
        //     } else {
        //         vm.createPost();
        //     }
        // }

        function submitPost() {
            vm.submittingPost = true;
            
            MyAppService.submitPost(vm.newpost, vm.file)
                .then(function (result) {
                    // console.log(result);
                    // vm.posts = result;
                    MyAppService.getPosts()
                        .then(function (result) {
                            // console.log(result);
                            vm.posts = result;                        
                        }).catch(function (err) {
                            console.log(err);
                        });

                        vm.newpost.text = " ";
                        vm.file = "";
                        vm.submittingPost = false;                        
                }).catch(function (err) {
                    console.log(err);
                    vm.submittingPost = false;                    
                });
        }

        function init() {
            MyAppService.getPosts()
                .then(function (result) {
                    // console.log(result);
                    vm.posts = result;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        function logout() {
            // console.log("ctrl login");
            MyAppService.logout()
                .then(function (result) {
                    console.log(result);
                    $state.go("login");
                }).catch(function (err) {
                    console.log(err);
                });
        }
    }

})();