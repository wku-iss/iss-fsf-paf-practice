(function () {
    "use strict";

    angular
        .module("MyApp")
        .controller("ProtectedCtrl", ProtectedCtrl);

    ProtectedCtrl.$inject = ["$scope", "MyAppService", "$state", "authService"];
    function ProtectedCtrl($scope, MyAppService, $state, authService) {
        var vm = this;
        vm.accounts = [];

        $scope.$on("event:auth-loginRequired", function () {
            console.log("401");
            $state.go("login");
        });

        $scope.$on("event:auth-loginConfirmed", function () {
            console.log("202");
            $state.go("protected");
        });

        $scope.$on("event:auth-forbidden", function () {
            console.log("403");
        });


        vm.editAccount = function (id) {
            // console.log(id);
            $state.go("edit", { id: id });
        }

        vm.init = function () {
            MyAppService.getProtectedAccounts()
                .then(function (result) {
                    // console.log(result);
                    vm.accounts = result;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        vm.init();

        vm.logout = function () {
            // console.log("ctrl login");
            MyAppService.logout()
                .then(function (result) {
                    console.log(result);
                    $state.go("login");
                }).catch(function (err) {
                    console.log(err);
                });
        }
    }

})();